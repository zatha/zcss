# Z# CSS3 Documentation v1.0.0

    Author       : SATHANANTHAN A
    Published on : 11.06.2020
    Source link  : https://gitlab.com/zatha/zcss.git
    Email        : sathananthanit15@gmail.com


This document clearly describe about the CSS3 predefined class name styles in my way. If you want to write a presentational layer quickly you can use it. I knew, there are lots of frameworks available for css like bootstrap, tailwind and etc. But why i’m do this? Just for customize and learn each and every part myself. Here, I just create this zcss.css for my convenience. Eventhough i hope i am using normal keywords to represent "css class selector names". So if you wish to use in your project, just do it.OK... All css properties are not listed because some properties are not handle in "class" tag. for ex: background-image, animation and some others.

## Intro
------------------------------------
Very first, we have to define the **_:root_** selector to store our variables. Unit values and color values are declared here. So you want to make changes in unit or color scheme just do in :root . You can declare your global styles in the **_\* , \*::after , \*::before_** selector. These are the two selectors declared in very first part of the zcss.css file. And all other selectors are declared one by one with grouping. See one by one later in this documentation.

> 1. How to access or set styles in html ?

    Simple. You have to link that zcss.css file in the html header tag.

```html
<head>
<title>...</title>
<link rel="stylesheet" type="text/css" href="zcss.css">
</head>
```
> 2. How to use zcss.css ?

    You must first declare the zcss.css file’s class name on the html element and then you can add your own class names. Why ?. Because easy to find and modify the class names.

```html
<div class=”grid grid-cols-12 your_class_name”>
    <div class=”col-span-3 p5 bred3 c1 abc”></div>
    <div class=”col-span-3 p5 bred4 d1 cde”></div>
    <div class=”col-span-3 p5 bred5 c2 efg”></div>
    <div class=”col-span-3 p5 bred6 d2 ghi”></div>
</div>
```
> 3. How to refer this ?

    I’ve separate and grouping the properties by related names and others are ordered in alphabetically.

## Prefix and Suffix
----------------------------------
Naming convention of class selector mentioned by following ways,
    
**prefix + class_name + suffix**

Ex: if you use grid you can use this way,

```html
<div class="sm_grid-cols-4"></div>
```
Here,

> Media breakpoints as **_prefix_**

**sm_** for small _(320px to 575px)_
**md_** for medium _(576px to 767px)_
**lg_** for large _(768px to 1199px)_
**xl_** for extra-large _(above 1200px)_

> class_name as **_selection_**

Mention class name as selection. Some classes are not having media breakpoints.

> **Note :** You can set class name without prefix(media breakpoints) but must include suffix.

> Value as **_suffix_**

mostly values append with **-** like _-4 , -left_ .

## Movie Time
------------------------------------
CSS3 Properties are order by alphabetically.

### **A**

> **align-content**

    prefix          -    yes
    class_name      -   .align-content-

    suffixes :

        start           -   flex-start
        center          -   center
        end             -   flex-end
        stretch         -   stretch
        between         -   space-between
        around          -   space-around

left side is suffix and right side is actual value.

Example : 

This is

```html
<div class="align-content-start"></div>
```
Equal to

``` css
.align-content-start{
    align-content: flex-start;
}
```

> **align-items**

    prefix          -   yes
    class_name      -   .align-items-

    suffixes :

        start       -   flex-start
        center      -   center
        end         -   flex-end
        baseline    -   baseline
        stretch     -   stretch

> **align-self**

    prefix          -   yes
    class_name      -   .align-self-

    suffixes :

        start       -   flex-start
        center      -   center
        end         -   flex-end
        baseline    -   baseline
        stretch     -   stretch

> **all**

    prefix          -   no
    class_name      -   .all-
    
    suffixes :

        ini         -   initial
        inh         -   inherit
        uns         -   unset

### **B**

> **backface-visibility**

    prefix          -   no
    class_name      -   .backface-

    suffixes :

        hidden      -   hidden
        visible     -   visible

### **Background**

> **background-attachment**

    prefix          -   yes
    class_name      -   .bg-

    suffixes :

        fixed       -   fixed
        local       -   local
        scroll      -   scroll

> **background-blend-mode**

    prefix          -   yes
    class_name      -   .bg-blend-

    suffixes :

        normal      -   normal
        multiply    -   multiply
        screen      -   screen
        overlay     -   overlay
        darken      -   darken
        lighten     -   lighten
        color-dodge -   color-dodge
        color-burn  -   color-burn
        hard-light  -   hard-light
        soft-light  -   soft-light
        difference  -   difference
        exclusion   -   exclusion
        hue         -   hue
        saturation  -   saturation
        color       -   color
        luminosity  -   luminosity

> **background-clip**

    prefix          -   yes
    class_name      -   .bg-c-

    suffixes :

        border-box  -   border-box
        content-box -   content-box
        padding-box -   padding-box

> **background-color**

    prefix          -   no
    class_name      -   .b

    suffixes :

        white, gray, black, silver, gold
        red, green, blue, orange, violet, yellow
        dark(red, green, blue, orange, violet, yellow)
        mango, guaya, indigo, tomato, pink, sky
        z-gray, z-green, z-violet

    All are having 9 variations except white.

For example :

```html
<div class="bgreen-6"></div>
```
```css
.bgreen-6{
    background-color: var(--green-6);
    /* --green-6 = hsl(120,100%,65%) */
}
```

> **background-origin**

    prefix          -   yes
    class_name      -   .bg-o-

    suffixes :

        boreder-box -   border-box
        content-box -   content-box
        padding-box -   padding-box

> **background-position**

    prefix          -   yes
    class_name      -   .bg-

    suffixes :

        left        -   left
        center      -   center
        right       -   right
        top         -   top
        bottom      -   bottom

> **background-repeat**

    prefix          -   yes
    class_name      -   .bg-

    suffixes :

        round       -   round
        space       -   space
        repeat      -   repeat
        no-repeat   -   no-repeat
        repeat-x    -   repeat-x
        repeat-y    -   repeat-y

> **background-size**

    prefix          -   yes
    class_name      -   .bg-

    suffixes :

        contain     -   contain
        cover       -   cover
        auto        -   auto

### **Border**

In Border property i've ready made set of values. those are,

    prefix      -   no

    3 types of border radius values :

        rounded-1   -   .125rem
        rounded-2   -   .5rem
        rounded     -   100%

    4 types of border styles :

        solid, dashed, dotted, double

    5 types of border width values :

        1   -   1px
        2   -   2px
        3   -   3px
        4   -   5px
        5   -   8px

    26 types of border colors :

        all colors including white.
    
Ok. How to use them ?

    .no-bor                 -   border: none;
    .no-rounded             -   border-radius: 0;
    .left-rounded-1         -   border-top-left-radius: .125rem;
                                border-bottom-left-radius: .125rem;
    .top-rounded            -   border-top-left-radius: 100%;
                                border-top-right-radius: 100%;
    .solid-bor-4            -   border-style: solid;
                                border-width: 5px;
    .dashed-left-bor-1      -   border-left-style: dashed;
                                border-left-width: 1px;
    .green-4-bor            -   border-color: var(--green-4);

> **border-collapse**

    prefix          -   no
    class_name      -   <nothing>

    suffixes :

        collapse    -   collaspse
        separate    -   seperate

> **box-decoration-break**

    prefix          -   no
    class_name      -   .box-

    suffixes :

        clone       -   clone
        slice       -   slice

> **box-shadow**

    prefix          -   no
    class_name      -   .shadow-

suffixes with classnames :

```css
.shadow-0{box-shadow: none;}
.shadow-1{box-shadow: 0 0 0 1px rgba(0,0,0, .05);}
.shadow-2{box-shadow: 0 1px 2px 0 rgba(0,0,0, .05);}
.shadow-3{box-shadow: 0 1px 3px 0 rgba(0,0,0, .1),
                      0 1px 2px 0 rgba(0,0,0, .06);}
.shadow-4{box-shadow: 0 4px 6px -1px rgba(0,0,0, .1),
                      0 2px 4px -1px rgba(0,0,0, .06);}
.shadow-5{box-shadow: 0 10px 15px -3px rgba(0,0,0, .1),
                      0 4px 6px -2px rgba(0,0,0, .05);}
.shadow-6{box-shadow: 0 20px 25px -5px rgba(0,0,0, .1),
                      0 10px 10px -5px rgba(0,0,0, .04);}
.shadow-7{box-shadow: 0 25px 50px -12px rgba(0,0,0, .25);}
.shadow-in{box-shadow: inset 0 2px 4px 0 rgba(0,0,0, .06);}
.shadow-out{box-shadow: 0 0 0 2px rgba(0,0,0, .2);}
```

> **box-sizing**

    prefix          -   no
    class_name      -   <nothing>

    suffixes :

        border-box  -   border-box
        content-box -   content-box

> **break-after**

    prefix          -   no
    class_name      -   .break-after-

    suffixes :

        always      -   always
        auto        -   auto
        avoid       -   avoid
        avoid-column -  avoid-column
        avoid-page  -   avoid-page        
        column      -   column
        left        -   left
        page        -   page
        right       -   right

> **break-before**

    prefix          -   no
    class_name      -   .break-before-

    suffixes :

        always      -   always
        auto        -   auto
        avoid       -   avoid
        avoid-column -  avoid-column
        avoid-page  -   avoid-page        
        column      -   column
        left        -   left
        page        -   page
        right       -   right

> **break-inside**

    prefix          -   no
    class_name      -   .break-inside-

    suffixes :

        auto        -   auto
        avoid       -   avoid
        avoid-column -  avoid-column
        avoid-page  -   avoid-page

### **Positioning**

> **bottom & left & top & right**

    prefix          -   yes
    class_names :
        
        .left-      -   left
        .right-     -   right
        .top-       -   top
        .bottom-    -   bottom

    suffixes :

        1       -   1%
        2       -   2%
        3       -   3%
        4       -   4%
        5       -   5%
        6       -   10%
        7       -   15%
        8       -   20%
        9       -   25%
        10      -   50%

### **C**

> **caption-side**

    prefix          -   no
    class_name      -   .caption-

    suffixes :

        top         -   top
        bottom      -   bottom

> **caret-color**

    prefix          -   no
    class_name      -   .caret-
    suffix :

        current     -   currentColor

> **clear**

    prefix          -   yes
    class_name      -   .clear-

    suffixes :

        left        -   left
        right       -   right
        both        -   both

> **color**

    In color scheme I am using 26 colors with each one having 9 variations except white color.
    color values are in hsl() type.

    25 x 9 = 225 + 1 colors. 

    prefix          -   no
    class_name      -   <nothing>

    suffixes :

        1. white
        2. gray, black, silver, gold
        3. red, green, blue, orange, violet, yellow
        4. dark(red, green, blue, orange, violet, yellow)
        5. mango, guaya, indigo, tomato, pink, sky
        6. z-gray, z-green, z-violet

    you can use like : red-4 | darkblue-4 | z-green-9 | sky-1

### **Column**

> **column-count**

    prefix          -   no
    class_name      -   .column-

    suffixes :

        2       -   column-count: 2;
        3       -   column-count: 3;
        4       -   column-count: 4;
        5       -   column-count: 5;

> **column-fill**

> **column-gap**

    prefix          -   no
    class_name      -   .column-gap-

    suffixes :

        1           -   1.5rem
        2           -   2rem
        3           -   2.5rem
        4           -   3rem

> **column-rule**

    prefix          -   no
    class_name      -   .<style>-rule-<width>[1-4]

    suffixes :

        .solid-rule-1   -   column-rule-style: solid;
                            column-rule-width: 1px;
        .dashed-rule-4  -   <as like as above>
        .dotted-rule-3  -   <as like as above>

**Note :** Here, no double style.

> **column-span**

    prefix          -   no
    class_name      - .column-span-

    suffixes :

        all         -   all
        none        -   none

> **column-width**

    prefix          -   no
    class_name      -   .column-

    suffixes :

        100         -   100px
        200         -   200px
        250         -   250px
        300         -   300px
        400         -   400px

> **columns**

    Short-hand property of column-count & column-width

    prefix          -   no
    class_name      -   .columns-

    suffixes :

        2           - columns: 2 250px;
        3           - columns: 3 250px;
        4           - columns: 4 250px;

> **content**

    prefix          -   no
    class_name      -   .content
    
    suffix :

        _           -   ""

> **counter-increment**

    prefix          -   no
    class_name      -   .count-

    suffixes :

        1           -   1
        2           -   2
        3           -   3
        4           -   4

> **counter-reset**

    prefix          -   no
    class_name      -   .count-reset-

    suffixes :

        0           -   0
        1           -   1

> **cursor**

    prefix          -   no
    class_name      -   .cursor-

    suffixes :

        alias           - alias
        all-scroll      - all-scroll
        auto            - auto
        cell            - cell
        context-menu    - context-menu
        copy            - copy
        crosshair       - crosshair
        default         - default
        grab            - grab
        grabbing        - grabbing
        help            - help
        move            - move
        no-drop         - no-drop
        none            - none
        not-allowed     - not-allowed
        zoom-in         - zoom-in
        zoom-out        - zoom-out
        pointer         - pointer
        progress        - progress
        text            - text
        vertical-text   - vertical-text
        wait            - wait

        column-resize   - col-resize
        row-resize      - row-resize
        n-resize        - n-resize
        e-resize        - e-resize
        s-resize        - sw-resize
        w-resize        - w-resize
        ew-resize       - ew-resize
        nw-resize       - nw-resize
        ne-resize       - ne-resize
        se-resize       - se-resize
        ns-resize       - ns-resize
        nesw-resizze    - nesw-resize
        nwse-resize     - nwse-resize

### **D**

> **direction**

    prefix          -   no
    class_name      -   .direction-

    suffixes :

        ltr         -   ltr
        rtl         -   rtl

> **display**

    prefix          -   yes
    class_name      -   <nothing>

    suffixes :

        inline          -   inline
        inline-block    -   inline-block
        block           -   block
        flex            -   flex
        inline-flex     -   inline-flex
        grid            -   grid
        display-none    -   none

### **E**

> **empty-cells**

    prefix          -   no
    class_name      -   .empty-cells-

    suffixes :

        show        -   show
        hide        -   hide

### **F**

### **Flex**

> **flex**

    prefix          -   yes
    class_name      -   .flex-
    
    suffixes :

        1   -   1   to    4   -   4

> **flex-basis**

    prefix          -   yes
    class_name      -   <value>-basis

    suffix :

        content-    -   flex-basis: content;

> **flex-direction**

    prefix          -   yes
    class_name      -   .flex-

    suffixes :

        row             -   row
        row-reverse     -   row-reverse
        column          -   column
        column-reverse  -   column-reverse

> **flex-grow**

    prefix          -   yes
    class_name      -   .grow-

    suffixes :

        1           -   1
        2           -   2
        3           -   3
        4           -   4

> **flex-shrink**

    prefix          -   yes
    class_name      -   .shrink-

    suffixes:

        0           -   0
        1           -   1
        2           -   2
        3           -   3

> **flex-wrap**

    prefix          -   yes
    class_name      -   <value>

    suffixes :

        wrap            -   wrap
        no-wrap         -   no-wrap
        wrap-reverse    -   wrap-reverse

> **float**

    prefix          -   yes
    class_name      -   .float-
    
    suffixes :

        left        -   left
        right       -   right
        start       -   inline-start
        end         -   inline-end

### **Font**

> **font-family**

    prefix          -   no
    class_name      -   <value>

    suffixes :

        sans-serif  -   sans-serif
        serif       -   serif
        monospace   -   monospace
        cursive     -   cursive
        fantacy     -   fantacy

> **font-kerning**

    prefix          -   no
    class_name      -   .kerning-

    suffixes :

        all         -   all
        none        -   none
        normal      -   normal

> **font-size**

    prefix          -   no
    class_name      -   .font-

    suffixes :

        xs          -   .75rem
        sm          -   .875rem
        md          -   1.125rem
        lg          -   1.25rem
        xl          -   1.5rem
        2xl         -   1.875rem
        3xl         -   2.25rem
        4xl         -   3rem
        5xl         -   4rem

> **font-size-adjust**

    prefix          -   no
    class_name      -   .font-adjust-

    suffixes :

        small       -   .75
        smaller     -   .5
        smallest    -   .25
        large       -   1.25
        larger      -   1.5
        largest     -   1.75

> **font-stretch**

    prefix          -   no
    class_name      -   .font-

    suffixes :

        condensed       -   condensed
        expanded        -   expanded
        x-condensed     -   extra-condensed
        x-expanded      -   extra-expanded
        narrower        -   narrower
        no-stretch      -   normal
        semi-condensed  -   semi-condensed
        semi-expanded   -   semi-expanded
        ultra-condensed -   ultra-condensed
        ultra-expanded  -   ultra-expanded
        wider           -   wider

> **font-style**

    prefix          -   no
    class_name      -   <nothing>

    suffixes :

        italic      -   italic
        no-italic   -   normal
        oblique     -   oblique

> **font-variant**

    prefix          -   no
    class_name      -   .font-

    suffixes :

        small-caps  -   small-caps
        normal      -   normal

> **font-variant-caps**

    prefix          -   no
    class_name      -   .font-

    suffixes :

        all-petite-caps -   all-petite-caps
        all-small-caps  -   all-small-caps
        caps-normal     -   normal
        petite-caps     -   petite-caps
        titling-caps    -   titling-caps
        unicase         -   unicase

> **font-weight**

    prefix          -   no
    class_name      -   .font-

    suffixes :

        100   -  100   to  900  -   900

### **G**

### **Grid**

There are 1 to 12 columns & 1 to 6 rows seperately in our grid. And also you can set grid column & row value using span, start & end. All grid properties having prefix.

    prefix          -   yes
    class_name      -   .grid-

Suffixes :

```css
.grid-cols-1{grid-template-columns: repeat(1, minmax(0,1fr));} /* 1 to 12 */
.grid-cols-none{grid-template-columns: none;}

.grid-rows-1{grid-template-rows: repeat(1, minmax(0,1fr));} /* 1 to 6 */
.grid-rows-none{grid-template-rows: none;}

```

> **grid-column**

    class_name      -   .col-

    suffixes :

        auto        -   auto
        span-1      -   grid-column: span 1;
                    .
                    .
                    .
        span-12     -   grid-column: span 12;

> **grid-column-start**

    class_name      -   .col-start-

    suffixes :

        auto        -   auto
        1           -   grid-column-start: 1;
                    .
                    .
                    .
        13          -   grid-column-start: 13;

> **grid-column-end**

    class_name      -   .col-end-

    suffixes :

        auto        -   auto
        1           -   grid-column-end: 1;
                    .
                    .
                    .
        13          -   grid-column-end: 13;

> **grid-row**

    class_name      -   .row-
    
    suffixes :

        auto        -   auto
        1           -   grid-row: span 1;
                    .
                    .
                    .
        6           -   grid-row: span 6;

> **grid-row-start**

    class_name      -   .row-start-
    
    suffixes :

        auto        -   auto
        1           -   grid-row-start: 1;
                    .
                    .
                    .
        7           -   grid-row-start: 7;

> **grid-row-end**

    class_name      -   .row-end-
    
    suffixes :

        auto        -   auto
        1           -   grid-row-end: 1;
                    .
                    .
                    .
        7           -   grid-row-end: 7;

> **row-gap & col-gap & gap**

    class_name      -   .row-gap- , .col-gap- , .gap-

    suffixes :

        0           -   0
        1           -   .25rem
        2           -   .5rem
        3           -   1rem
        4           -   2rem
        5           -   4rem
        6           -   6rem
        7           -   8rem
        8           -   10rem

> **grid-auto-flow**

    class_name      -   .grid-flow-

    suffixes :

        row         -   row
        col         -   column
        row-dense   -   row dense;
        col-dense   -   column dense;

### **H**

> **height & width (max & min)**

    prefix          -   yes
    class_names :

        .h          -   height
        .min-h      -   min-height
        .max-h      -   max-height

        .w          -   width
        .min-w      -   min-width
        .max-width  -   max-width

    suffixes :

        1   -   1vh  to  9  -   9vh (only for .h)

        1/10        -   10 vh | vw
        1/5         -   20 vh | vw
        1/4         -   25 vh | vw
        3/10        -   30 vh | vw
        1/3         -   33 vh | vw
        2/5         -   40 vh | vw
        1/2         -   50 vh | vw
        3/5         -   60 vh | vw
        2/3         -   66 vh | vw
        7/10        -   70 vh | vw
        3/4         -   75 vh | vw
        4/5         -   80 vh | vw
        9/10        -   90 vh | vw
        100         -   100 vh | vw

**Message :** you can also use width value as percentage by adding **p** as suffix.

Example :
```html
<div class="w1/10p"></div>
```
```css
.w1\/10p{width: 10%;}
```

### **J**

> **justify-content**

    prefix          -   yes
    class_name      -   .justify-

    suffixes :

        start       -   start
        center      -   center
        end         -   end
        left        -   left
        right       -   right
        flex-start  -   flex-start
        flex-end    -   flex-end
        evenly      -   space-evenly
        between     -   space-between
        around      -   space-around
        stretch     -   stretch
        baseline    -   baseline

### **L**

> **letter-spacing**

    prefix          -   no
    class_name      -   <nothing>

    suffixes :

        tighter     -   -0.05rem
        tight       -   -0.025rem
        wide        -   0.025rem
        wider       -   0.05rem
        widest      -   .1rem

> **line-break**

    prefix          -   no
    class_name      -   .line-

    suffixes :

        auto        -   auto
        loose       -   loose
        normal      -   normal
        strict      -   strict

> **line-height**

    prefix          -   no
    class_name      -   .leading-

    suffixes :

        none        -   1
        xs          -   1.25
        sm          -   1.375
        normal      -   1.5
        md          -   1.625
        lg          -   1.875
        xl          -   2
        2xl         -   2.25
        3xl         -   2.5
        4xl         -   2.75
        5xl         -   3

> **list-style**

    prefix          -   no
    class_name      -   .list-

    suffixes :

        none                -   none
        square              -   square
        decimal             -   decimal
        low-alpha           -   lower-alpha
        low-roman           -   lower-roman
        up-alpha            -   upper-alpha
        up-roman            -   upper-roman
        in-square           -   inside square;
        in-decimal          -   inside decimal;
        in-low-alpha        -   inside lower-alpha;
        in-low-roman        -   inside lower-roman;
        in-up-alpha         -   inside upper-alpha;
        in-up-roman         -   inside upper-roman;

### **M**

> **margin & padding**

    prefix          -   no
    class_names :

        .m          -   margin
        .mt         -   margin-top
        .mb         -   margin-bottom
        .ml         -   margin-left
        .mr         -   margin-right

        .p          -   padding
        .pt         -   padding-top
        .pb         -   padding-bottom
        .pl         -   padding-left
        .pr         -   padding-right

        shorthand property

        .mx         -   margin-left
                        margin-right
        .my         -   margin-top
                        margin-bottom
    
        .px         -   padding-left
                        padding-right
        .py         -   padding-top
                        padding-bottom

    suffixes :

        0           -   0
        a           -   auto
        0a          -   0 auto;
        a0          -   auto 0;

        1           -   .25rem
        2           -   .5rem
        3           -   1rem
        4           -   2rem
        5           -   4rem
        6           -   6rem
        7           -   8rem
        8           -   10rem

        1p          -   1%
        2p          -   2%
        3p          -   3%
        4p          -   4%
        5P          -   5%
        6p          -   10%
        7p          -   15%
        8p          -   20%

**Note :** padding only have 0 not _a , 0a, a0_. And other values are same for both margin & padding.

### **O**

> **object-fit**

    prefix          -   no
    class_name      -   .object-

    suffixes :

        contain     -   contain
        cover       -   cover
        fill        -   fill
        scale-down  -   scale-down

> **object-position**

    prefix          -   no
    class_name      -   .object-

    suffixes :

        top         -   top
        bottom      -   bottom
        left        -   left
        right       -   right
        center      -   center

> **opacity**

    prefix          -   no
    class_name      -   .opacity-

    suffixes :

        1  -  0.1   to    9  -  0.9

> **order**

    prefix          -   yes
    class_name      -   .order-

    suffixes :

        1  -  1     to    4  -  4

> **outline**

    prefix          -   no
    class_name      -   .<value>-outline

    suffixes :

        no              -   none
        dashed          -   dashed
        dotted          -   dotted
        double          -   double
        solid           -   solid
        inset           -   inset
        outset          -   outset
        current-color   -   outline-color: currentColor;

    class_name      -   .outline-

    suffixes :

        1       -   outline-width: 1px;
        2       -   outline-width: 2px;
        3       -   outline-width: 4px;
        4       -   outline-width: 8px;

        off-1   -   outline-width: 1px;
        off-2   -   outline-width: 2px;
        off-3   -   outline-width: 4px;
        off-4   -   outline-width: 8px;

> **overflow**

    prefix           -   no
    class_names :

        .overflow-   -   overflow
        .overflow-x- -   overflow-x
        .overflow-y- -   overflow-y

    suffixes :

        <no-value>   -   auto
        scroll       -   scroll
        visible      -   visible

    for overflow-wrap :

        class_name   -   .overflow-

        break        -   break-word
        normal       -   normal

    for hidden value :

        .no-overflow
        .no-overflow-x
        .no-overflow-y

### **P**

> **page**

    prefix          -   no
    class_names :

        .page-after-    -   page-break-after
        .page-before-   -   page-break-before

    suffixes :

        auto        -   auto
        aviod       -   avoid
        always      -   always
        left        -   left
        right       -   right

    for .page-inside-   -   page-break-inside

        auto        -   auto
        avoid       -   avoid

**Note : No** always,left,right in _page-break-inside_

> **pointer-events**

    prefix          -   no
    class_name      -   .pointer-

    suffixes :

        all             -   all
        fill            -   fill
        none            -   none
        painted         -   painted
        stroke          -   stroke
        visible         -   visible
        visibleFill     -   visibleFill
        visiblePainted  -   visiblePainted
        visibleStroke   -   visibleStroke

> **position**

    prefix          -   yes
    class_name      -   <value>

    suffixes :

        absolute    -   absolute
        relative    -   relative
        static      -   static
        sticky      -   sticky

### **R**

> **resize**

    prefix          -   no
    class_name      -   .resize-

    suffixes :

        both        -   both
        horizontal  -   horizontal
        vertical    -   vertical
        none        -   none

### **S**

> **scroll-behaviour**

    prefix          -   no
    class_name      -   .scroll-

    suffixes :

        auto        -   auto
        smooth      -   smooth

### **T**

### **Text**

> **text-align**

    prefix          -   no
    class_name      -   .text-

    suffixes :

        left        -   left
        right       -   right
        center      -   center
        justify     -   justify

> **text-align-last**

    prefix          -   no
    class_name      -   .text-last-

    suffixes :

        left        -   left
        right       -   right
        center      -   center
        justify     -   justify

> **text-combine-upright**

    prefix          -   no
    class_name      -   .combine-

    suffixes :

        none        -   none
        all         -   all
        2           -   2
        4           -   4

> **text-decoration**

    prefix          -   no
    class_name      -   <value>

    suffixes :

        underline       -   underline
        no-underline    -   none
        line-through    -   line-through
        dashed          -   dashed
        dotted          -   dotted
        double          -   double
        current-color   -   text-decoration-color: currentColor;

> **text-indent**

    prefix          -   no
    class_name      -   .indent (3rem)

    suffix : <no_suffix>

> **text-justify**

    prefix          -   no
    class_name      -   .justify-

    suffixes :

        auto            -   auto
        distribute      -   distribute
        all-lines       -   distribute-all-lines
        inter-word      -   inter-word
        kashida         -   kashida
        newspaper       -   newspaper
        inter-cluster   -   inter-cluster
        inter-ideograph -   inter-ideograph

> **text-orientation**

    prefix          -   no
    class_name      -   .orientation

    suffixes :

        sideways        -   sideways
        sideways-right  -   sideways-right
        upright         -   upright

> **text-overflow**

    prefix          -   no
    class_name      -   .overflow-

    suffixes :

        clip        -   clip
        ellipsis    -   ellipsis

> **text-shadow**

    prefix          -   no
    class_name      -   .text-shadow-

    suffixes :

        1       -   0   0   3px #000000;
        2       -   2px 2px 4px #000000;

> **text-transform**

    prefix          -   no
    class_name      -   <value>

    suffixes :

        capitalize  -   capitalize
        lowercase   -   lowercase
        uppercase   -   uppercase
        normal-case -   none

> **text-underline-position**

    prefix          -   no
    class_name      -   .underline-

    suffixes :

        above       -   above
        below       -   below

### **U**

> **user-select**

    prefix          -   no
    class_name      -   .user-select-

    suffixes :

        all         -   all
        auto        -   auto
        none        -   none
        contain     -   contain
        text        -   text

### **V**

> **vertical-align**

    prefix          -   no
    class_name      -   .vertical-

    suffixes :

        auto        -   auto
        baseline    -   baseline
        bottom      -   bottom
        top         -   top
        middle      -   middle
        sub         -   sub
        super       -   super
        text-top    -   text-top
        text-bottom -   text-bottom

> **visibility**

    prefix          -   no
    class_name      -   .visibility-

    suffixes :

        collapse    -   collapse
        visible     -   visible
        hidden      -   hidden

### **W**

> **white-space**

    prefix          -   n-
    class_name      -   .space-

    suffixes :

        no-space    -   normal (don't add class_name)
        no-wrap     -   nowrap
        pre         -   pre
        pre-line    -   pre-line
        pre-wrap    -   pre-wrap    

> **word-break**

    prefix          -   no
    class_name      -   .word-

    suffixes :

        no-word-break   -   normal (don't add class_name)
        break           -   break-all
        keep            -   keep-all

> **word-spacing**

    prefix          -   no
    class_name      -   .word-

    suffixes :

        wide        -   .25rem
        wider       -   .5rem
        widest      -   .75rem

> **word-wrap**

    prefix          -   no
    class_name      -   .word-

    suffixes :

        no-word-wrap    -   normal (don't add class_name)
        wrap            -   break-word

> **writing-mode**

    prefix          -   no
    class_name      -   .write-

    suffixes :

        slr         -   sideways-lr
        srl         -   sideways-rl
        vlr         -   vertical-lr
        vrl         -   vertical-rl
        htb         -   horizontal-tb

### **Z**

> **z-index**

    prefix          -   no
    class_name      -   .z

    suffixes :

        -5  -  -5     to    5  -  5


**Bye...** _keep in touch_ ,